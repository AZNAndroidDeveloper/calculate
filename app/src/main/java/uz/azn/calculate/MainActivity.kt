package uz.azn.calculate

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import uz.azn.calculate.databinding.ActivityMainBinding
import java.lang.StringBuilder
import kotlin.math.log

class MainActivity : AppCompatActivity() {

    val stringBuilder: StringBuilder = StringBuilder()
    var tempStringBuilder: StringBuilder = StringBuilder()
    private var value1: Double = 0.0
    private var value2: Double = 0.0
    private var operation: String = ""
    private var finalResult: Double = 0.0
    val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        binding.oneButton.setOnClickListener { onClick(binding.oneButton) }
        binding.twoButton.setOnClickListener { onClick(binding.twoButton) }
        binding.threeButton.setOnClickListener { onClick(binding.threeButton) }
        binding.fourButton.setOnClickListener { onClick(binding.fourButton) }
        binding.fiveButton.setOnClickListener { onClick(binding.fiveButton) }
        binding.sixButton.setOnClickListener { onClick(binding.sixButton) }
        binding.sevenButton.setOnClickListener { onClick(binding.sevenButton) }
        binding.eightButton.setOnClickListener { onClick(binding.eightButton) }
        binding.nineButton.setOnClickListener { onClick(binding.nineButton) }
        binding.zeroButton.setOnClickListener { onClick(binding.zeroButton) }
        binding.pointButton.setOnClickListener { onClick(binding.pointButton) }
        binding.cButton.setOnClickListener { onClick(binding.cButton) }
        binding.deleteButton.setOnClickListener { onClick(binding.deleteButton) }

        //Math operation buttons
        binding.plusButton.setOnClickListener { btnMath(binding.plusButton) }
        binding.minusButton.setOnClickListener { btnMath(binding.minusButton) }
        binding.multiplyButton.setOnClickListener { btnMath(binding.multiplyButton) }
        binding.divideButton.setOnClickListener { btnMath(binding.divideButton) }
        binding.resultButton.setOnClickListener { btnEquel() }
        binding.resultTextView.setOnClickListener{resultTextView()}
        binding.ceButton.setOnClickListener {ceButton()  }
        binding.percentButton.setOnClickListener { percentButton() }

    }

    fun onClick(view: View) {
        val id = view.id
       var   value = binding.editText.text.toString()
        when (id) {
            binding.oneButton.id -> {
                value += "1"
                stringBuilder.append("1")
                tempStringBuilder.append("1")
            }
            binding.twoButton.id -> {
                value += "2"
                stringBuilder.append("2")
                tempStringBuilder.append("2")

            }
            binding.threeButton.id -> {
                value += "3"
                stringBuilder.append("3")
                tempStringBuilder.append("3")

            }
            binding.fourButton.id -> {
                value += "4"
                stringBuilder.append("4")
                tempStringBuilder.append("4")

            }
            binding.fiveButton.id -> {
                value += "5"
                stringBuilder.append("5")
                tempStringBuilder.append("5")
            }
            binding.sixButton.id -> {
                value += "6"
                stringBuilder.append("6")
                tempStringBuilder.append("6")
            }
            binding.sevenButton.id -> {
                value += "7"
                stringBuilder.append("7")
                tempStringBuilder.append("7")
            }
            binding.eightButton.id -> {
                value += "8"
                stringBuilder.append("8")
                tempStringBuilder.append("8")
            }
            binding.nineButton.id -> {
                value += "9"
                stringBuilder.append("9")
                tempStringBuilder.append("9")

            }
            binding.zeroButton.id -> {
                value += "0"
                stringBuilder.append("0")
                tempStringBuilder.append("0")
            }
            binding.pointButton.id -> {
                value += "."
                stringBuilder.append(".")
                tempStringBuilder.append(".")

            }
            binding.cButton.id -> {

                value = ""
                stringBuilder.clear()
                tempStringBuilder.clear();
            }
            binding.deleteButton.id -> {
                value = binding.editText.text.toString()

                if (value.isNotEmpty()) {
                    value = value.substring(0, value.length - 1)
                }
                if (stringBuilder.isNotEmpty()) {
                    stringBuilder.deleteCharAt(stringBuilder.length - 1)
                    if (stringBuilder.contains("=")) {
                        value = ""
                        stringBuilder.clear()
                    }
                }

            }
        }
        binding.editText.setText(value)
    }
    private fun isMathOperation(c: Char):Boolean {
        if(c == '+' || c=='-' || c == '/' || c == '*') return true;
        return false;
    }

    fun btnMath(view: View) {
        val id = view.id
        when (id) {
            binding.multiplyButton.id -> {
                if(stringBuilder.isNotEmpty() && !isMathOperation(stringBuilder[stringBuilder.length - 1])) {
                    stringBuilder.append("*");
                    }
            }
            binding.divideButton.id -> {
                if(stringBuilder.isNotEmpty() && !isMathOperation(stringBuilder[stringBuilder.length - 1])) {
                    stringBuilder.append("/");
                }
            }
            binding.minusButton.id -> {
                if(stringBuilder.isNotEmpty() && !isMathOperation(stringBuilder[stringBuilder.length - 1])) {
                    stringBuilder.append("-")
                }
                if(stringBuilder.isEmpty()) stringBuilder.append("-")
            }
            binding.plusButton.id -> {
                if(stringBuilder.isNotEmpty() && !isMathOperation(stringBuilder[stringBuilder.length - 1])) {
                    stringBuilder.append("+")
                }
            }
        }
        if(stringBuilder.isNotEmpty()) {
            binding.editText.setText(stringBuilder.toString())
        }
    }

    private fun getCharCode(a: Char):Int {
        return a.toInt()
    }

    private fun btnEquel(){
        if(stringBuilder.isEmpty()) return
        if(stringBuilder.isNotEmpty() && isMathOperation(stringBuilder[stringBuilder.length - 1])) return
        binding.resultTextView.text = stringBuilder.toString()
        if ( stringBuilder.isNotEmpty() && !isMathOperation(stringBuilder[0]))  {
            stringBuilder.insert(0, '+');
        }
        stringBuilder.append('+');
        Log.d("stringEqual: ", stringBuilder.toString());
        var a: MutableList<Double> = arrayListOf()
        var num = 0.0;
        var sign = 1;
        var lastOperation = '+';
        for(i in stringBuilder.indices) {
            if (isMathOperation(stringBuilder[i])) {
                if(i > 0) {
                    if(lastOperation == '-') {
                        a.add(-1 * num);
                    } else if(lastOperation == '+') {
                        a.add(num)
                    } else if(lastOperation == '*') {
                        a[a.size-1] *= num;
                    } else if(lastOperation == '/') {
                        a[a.size-1] /= num;
                    }
                }
                num = 0.0
                lastOperation = stringBuilder[i];
            } else {
                num = num * 10 + getCharCode(stringBuilder[i]) - 48;
            }
        }
        var result = 0.0
        for(item in a) {
            result+=item
        }
        Log.d("arrayresult", a.toString())
        Log.d("result", result.toString())

        stringBuilder.append(result)
        binding.editText.setText(" = ${result.toString()}")
        tempStringBuilder.clear()

    }

    private fun resultTextView(){
        binding.editText.setText("")
       binding.resultTextView.setOnClickListener { binding.editText.setText(binding.resultTextView.text)  }
    }

    private fun ceButton(){
        binding.editText.text.clear()
        binding.resultTextView.text =""
        tempStringBuilder.clear()
        stringBuilder.clear()

    }

    private fun percentButton(){

        val d:Double = value1 / 100
        binding.editText.setText("=${d}")
        binding.resultTextView.text = "$d"
        tempStringBuilder.clear()
        stringBuilder.clear()

    }

}